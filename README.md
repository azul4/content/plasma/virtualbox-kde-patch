# virtualbox-kde-patch

[![License: GPL v3](https://img.shields.io/badge/License-GPL%20v3-blue.svg)](http://www.gnu.org/licenses/gpl-3.0)

Fix file selection dialog issue.

It is now possible to use the mouse to select a file.

<br><br>
How to clone this repository:

```
git clone https://gitlab.com/azul4/content/plasma/virtualbox-kde-patch.git
```
<br><br>
# How to use

Once the patch is installed, run VirtualBox using the new shortcut created by this installer: **virtualbox-kde**.

